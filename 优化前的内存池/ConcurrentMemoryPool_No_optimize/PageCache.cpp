#include"PageCache.h"

PageCache PageCache::_PageCacheInstacne;
Span* PageCache::NewSpan(size_t K)
{
	assert(K > 0 && K < MAXPAGEBUCKETS);

	//检测第K个桶有无span
	if (!_spanlists[K].empty())
	{
		//弹出一个span
		return _spanlists[K].PopFront();
	}
	//往后面扫描最近的桶有无span，如果有可以把它切分为K 和 sum - k 2块
	for (size_t i = K + 1; i < MAXPAGEBUCKETS; i++)
	{
		if (!_spanlists[i].empty())
		{
			Span* Kspan = new Span;
			//切分span
			Span* span = _spanlists[i].PopFront();//最开始脱离下来的span
			//修改要返回的Kspan的起始页号
			Kspan->_pageId = span->_pageId;
			//修改要返回的Kspan页数
			Kspan->_n = K;
			/*--修改原来被切下来的span的页号和页数--*/
			span->_pageId += K;
			span->_n -= K;

			//存储span的首尾页号与span的映射关系，方便中心缓存回收内存时进行的合并查找
			_idSpanMap[span->_pageId] = span;
			_idSpanMap[span->_pageId + span->_n -1] = span;

			/*---------------------------------*/
			//将剩余的span挂到对应的桶下面
			_spanlists[span->_n].PushFront(span);
			//建立id和span的映射关系
			for (PAGE_ID i = 0; i < Kspan->_n; i++)
			{
				_idSpanMap[Kspan->_pageId + i] = Kspan;
			}
			return Kspan;
		}
	}
	//PageCache全为空,此时向系统申请一个128kb的页
	Span* maxSpan = new Span;
	//计算页号
	void* ptr = SystemAlloc(MAXPAGEBUCKETS - 1);
	maxSpan->_pageId = (PAGE_ID)ptr >> PAGE_SHIFT;
	maxSpan->_n = MAXPAGEBUCKETS - 1;
	_spanlists[maxSpan->_n].PushFront(maxSpan);
	//递归调用一次自己!
	return NewSpan(K);
}

Span* PageCache::GetObjToSpanMap(void* obj)
{
	PAGE_ID id = ((PAGE_ID)obj >> PAGE_SHIFT);
	std::unordered_map<PAGE_ID,Span*>::iterator it =  _idSpanMap.find(id);
	if (it != _idSpanMap.end())
	{
		return it->second;
	}
	else
	{
		assert(false);
		return nullptr;
	}
}
std::mutex& PageCache:: GetMutex()
{
	return _PageMutex;
}
//回收中心缓存的Span
void PageCache::ReleaseSpanToPageCache(Span* span)
{
	//向前合并span
	while (true)
	{
		//对span前后的页，尝试进行合并，缓解内存碎片问题
		PAGE_ID prevId = span->_pageId - 1;//查找传入的span前面的span
		std::unordered_map<PAGE_ID, Span*>::iterator it = _idSpanMap.find(prevId);
		//前面的页号没有了，停止合并
		if (it == _idSpanMap.end())
		{
			break;
		}
		Span* prevSpan = it->second;
		//遇到在使用的span，停止合并
		if (prevSpan->_isUse == true)
		{
			break;
		}
		//合并出超过128页的span，停止合并
		if (prevSpan->_n + span->_n > MAXPAGEBUCKETS - 1)
		{
			break;
		}

		//合并span
		span->_pageId = prevSpan->_pageId;
		span->_n += prevSpan->_n;
		//脱离prevSpan
		_spanlists[prevSpan->_n].erase(prevSpan);
		delete prevSpan;
	}
	//向后合并span
	while (true)
	{
		PAGE_ID nextId = span->_pageId + span->_n;
		std::unordered_map<PAGE_ID, Span*>::iterator it = _idSpanMap.find(nextId);
		if (it == _idSpanMap.end())
		{
			break;
		}
		Span* nextSpan = it->second;
		if (nextSpan->_isUse == true)
		{
			break;
		}

		if (nextSpan->_n + span->_n > MAXPAGEBUCKETS - 1)
		{
			break;
		}
		span->_n += nextSpan->_n;
		_spanlists[nextSpan->_n].erase(nextSpan);
		delete nextSpan;
	}
	_spanlists[span->_n].PushFront(span);
	span->_isUse = false;
	//重新建立映射
	_idSpanMap[span->_pageId] = span;
	_idSpanMap[span->_pageId + span->_n - 1] = span;
}