#pragma once
#include"Common.h"
#include"CentralCache.h"
class ThreadCache
{
public:

	void* AllocMemory(size_t size);
	void DelAllocMemory(void* obj, size_t size);
	//从中心缓存中获取对象
	void* GetFromCentralCache(size_t index, size_t size);
	//归还内存块到中心缓存
	void ListToLong(FreeList& list, size_t size);
private:
	FreeList _FL[MAXFREELIST];
};
//创建每个线程私有的pTLSThreadCache指针
static __declspec(thread) ThreadCache* pTLSThreadCache = nullptr;