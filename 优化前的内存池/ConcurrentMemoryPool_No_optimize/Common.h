#pragma once
#include<iostream>
#include<ctime>
#include<cassert>
#include<vector>
#include<thread>
#include<mutex>
#include<algorithm>
#include<unordered_map>
using std::cin;
using std::cout;
using std::endl;

/*由于32位和64位系统的进程地址空间不同，后者太大，size_t无法存下页号上限*/
//采用条件编译
#ifdef _WIN64
typedef unsigned long long PAGE_ID;
#elif _WIN32
typedef size_t PAGE_ID;
#elif linux
typedef unsigned long long PAGE_ID;
#elif __MACH
typedef unsigned long long PAGE_ID;
#endif
#ifdef _WIN64
#include<windows.h>
#elif _WIN32
#include<windows.h>
#elif linux
//
#endif
const static size_t MAX_BYTES = 1 << 19;
const static size_t MAXFREELIST = 208;
const static size_t MAXPAGEBUCKETS = 129;
const static size_t PAGE_SHIFT = 13;
//去系统堆上直接申请空间
inline static void* SystemAlloc(size_t N)//传入的是页数
{
#ifdef _WIN64
	void* _ptr = VirtualAlloc(NULL, N << PAGE_SHIFT, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
#elif _WIN32
	void* _ptr = VirtualAlloc(NULL, N << PAGE_SHIFT, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
#elif linux
	//linux环境
#endif

	if (_ptr == nullptr) throw std::bad_alloc();
	return _ptr;
}
/*--封装自由链表--*/
class FreeList
{
public:
	//取出obj的前4/8个字节存放地址
	static void*& NextObj(void* obj)
	{
		return *(void**)obj;
	}
public:
	void PushAllocMem(void* obj)
	{
		assert(obj);
		/*--头插——-*/
		NextObj(obj) = _freeList;
		_freeList = obj;
		++_size;
	}
	//一段范围内的OBJ插入
	void PushRandge(void* start, void* end, size_t n)
	{
		NextObj(end) = _freeList;
		_freeList = start;
		_size += n;
	}
	void PopRange(void*& start, void*& end, size_t num)
	{
		assert(num >= _size);
		start = _freeList;
		end = start;
		for (size_t i = 0; i < num - 1; i++)
		{
			end = NextObj(end);
		}
		_freeList = NextObj(end);
		NextObj(end) = nullptr;
		_size -= num;
	}
	void* PopAllocateMem()
	{
		assert(_freeList);
		/*--头删--*/
		void* next = NextObj(_freeList);
		void* AllocMem = _freeList;
		_freeList = next;
		--_size;
		return AllocMem;
	}
	bool empty()
	{
		return _freeList == nullptr;
	}
	size_t& MaxSize()
	{
		return _MaxSize;
	}
	size_t size()
	{
		return _size;
	}
private:
	void* _freeList = nullptr;
	size_t _MaxSize = 1;
	size_t _size = 0;
};

/*--计算对象大小的对齐映射规则--*/
class SizeMapClass
{
private:
	static size_t _AlignSize(size_t size, size_t AlignNum)
	{
		size_t alignsize = size;
		size_t ModSize = size % AlignNum;
		if (ModSize)
		{
			alignsize += (AlignNum - ModSize);
		}
		return alignsize;
	}
	/*计算在某个链表区间的桶数*/
	static size_t _AlignBucket(size_t size, size_t AlignNum)
	{
		if (size % AlignNum)
		{
			return size / AlignNum;
		}
		else
		{
			return size / AlignNum - 1;
		}
	}
public:
	/*--定义静态成员函数,方便外界调用--*/
	static size_t AlignSize(size_t size)
	{
		if (size <= 128)
		{
			return _AlignSize(size, 8);
		}
		else if (size <= 1024)
		{
			return _AlignSize(size - 128, 16);
		}
		else if (size <= ((size_t)1 << 13))
		{
			return _AlignSize(size - 1024, 128);
		}
		else if (size <= ((size_t)1 << 16))
		{
			return _AlignSize(size - ((size_t)1 << 13), 1024);
		}
		else if (size <= ((size_t)1 << 18))
		{
			return _AlignSize(size - ((size_t)1 << 16), (size_t)1 << 13);
		}
		else
		{
			assert(false);
			return -1;
		}
	}
	/*获取在自由链表中对应的桶的下标*/
	static size_t AlignBucket(size_t size)
	{
		assert(size <= MAX_BYTES);
		static size_t glistbucket[4] = { 16,56,56,56 };
		/*0-128*/
		if (size <= 128)
		{
			return _AlignBucket(size, 8);
		}
		/*129-1024*/
		else if (size <= 1024)
		{
			return _AlignBucket(size, 16) + glistbucket[0];
		}
		/*1025-8*1024*/
		else if (size <= ((size_t)1 << 13))
		{
			return _AlignBucket(size, 128) + glistbucket[1];
		}
		/*8*1024+1-64*1024*/
		else if (size <= ((size_t)1 << 16))
		{
			return _AlignBucket(size, 1024) + glistbucket[2];
		}
		/*64*1024+1-256*1024*/
		else if (size <= ((size_t)1 << 18))
		{
			return _AlignBucket(size, ((size_t)1 << 13)) + glistbucket[3];
		}
		else
		{
			assert(false);
			return -1;
		}
	}
	//threadCache一次性从中心缓存中获取多少个内存块
	static size_t TdGetForCenCache(size_t size)
	{
		assert(size);
		size_t numsize = MAX_BYTES / size;
		if (numsize < 2)//申请的size很大时
		{
			numsize = 2;
		}
		else if (numsize > 512)//申请的size很小时
		{
			numsize = 512;
		}
		return numsize;
	}
	//PageCache从系统中一次申请几页page
	static size_t PageGetForOs(size_t size)
	{
		size_t alignsize = TdGetForCenCache(size);
		size_t alignPageNum = alignsize * size;//总字节大小

		//总字节转换页的num
		alignPageNum >>= PAGE_SHIFT;//等同于alignPageNum /= 8 -->1页 = 8kb
		if (alignPageNum == 0)
		{
			alignPageNum = 1;
		}
		return alignPageNum;
	}
};
//管理span结构的组织
struct Span
{
	PAGE_ID _pageId = 0;//页号  
	size_t _n = 0;//页数
	Span* _prev = nullptr;
	Span* _next = nullptr;
	size_t _useCount = 0;
	void* _freeList = nullptr;//切割好的待分配的小块内存

	bool _isUse = false;//是否在被使用
};

/*--本质是一个带头双向循环链表,用于挂起Span--*/
class SpanList
{
public:
	SpanList()
	{
		_phead = new Span();
		_phead->_next = _phead;
		_phead->_prev = _phead;
	}
	//原生迭代器
	Span* begin()
	{
		return _phead->_next;
	}
	Span* end()
	{
		return _phead;
	}
	void PushFront(Span* span)
	{
		insert(begin(), span);
	}
	void insert(Span* pos, Span* newSpan)
	{
		assert(pos && newSpan);
		Span* prev = pos->_prev;
		prev->_next = newSpan;
		/*修改newSpan指向*/
		newSpan->_next = pos;
		newSpan->_prev = prev;
		/*修改pos指向*/
		pos->_prev = newSpan;
	}
	//?
	void erase(Span* pos)
	{
		assert(pos && pos != _phead);
		Span* prev = pos->_prev;
		Span* next = pos->_next;
		prev->_next = next;
		next->_prev = prev;
	}
	bool empty()
	{
		return _phead == _phead->_next;
	}
	Span* PopFront()
	{
		Span* span = begin();
		erase(span);
		return span;
	}
private:
	Span* _phead = nullptr;
public:
	std::mutex _mutex;//桶锁
};