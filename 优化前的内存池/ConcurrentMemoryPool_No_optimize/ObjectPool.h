#pragma once
#include"Common.h"
#ifdef _WIN64
#include<windows.h>
#elif _WIN32
#include<windows.h>
#endif
template<typename T>
class ObjectPool
{
public:
	ObjectPool() {}
	T* New()
	{
		T* OBJ = nullptr;
		/*---优先使用归还的内存，在freeList中---*/
		if (_freeList)
		{
			void* next = *(void**)_freeList;
			OBJ = (T*)_freeList;
			_freeList = next;
		}
		else
		{
			/*---当内存池剩余字节不足T类型对象的字节时---*/
			if (_RemainBytes < sizeof(T))
			{
				_RemainBytes = (size_t)128 * 1024;
				_memory = (char*)SystemAlloc(_RemainBytes >> 13);
				if (_memory == nullptr) throw std::bad_alloc();
			}

			OBJ = (T*)_memory;
			/*---特殊情况-->T字节数小于指针字节时*/
			size_t OBJSize = sizeof(T) < sizeof(void*) ? sizeof(void*) : sizeof(T);
			_memory += OBJSize;
			_RemainBytes -= OBJSize;
		}
		/*---对已经申请好的空间进行显示调用构造函数---*/
		new(OBJ)T;
		return OBJ;
	}
	void Delete(T* OBJ)
	{
		/*---显示调用T对象的析构函数---*/
		OBJ->~T();
		/*---将归还的内存通过freeList连接起来---*/
		if (_freeList == nullptr)
		{
			_freeList = OBJ;
			/*---将OBJ的前4 或者 8个字节存放nullptr地址---*/
			/*---将OBJ强转为二级指针，避开32/64位环境的指针大小判断---*/
			*(void**)OBJ = nullptr;
		}
		else
		{
			/*---freeList头插---*/
			*(void**)OBJ = _freeList;
			_freeList = OBJ;
		}
	}
private:
	char* _memory = nullptr;/*--指向大块内存的指针--*/
	size_t _RemainBytes = 0;/*--内存池每次切割后剩余的字节--*/
	void* _freeList = nullptr;/*--自由链表，用于归还内存--*/
};