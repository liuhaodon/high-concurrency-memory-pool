#include"ObjectPool.h"
#include"Common.h"
#include "ConcurrentAllocMem.h"
#include "CentralCache.h"
#include "PageCache.h"


void TestConCurrentAlloc()
{
	int cnt = 5;
	while (cnt--)
	{
		for (size_t i = 0; i < 100000; i++)
		{
			int* ptr = (int*)ConcurrentAlloc(4);
			*ptr = i;
			cout << *ptr << endl;
			ConcurrentFree(ptr, 100);
		}
	}
}
int main()
{
	TestConCurrentAlloc();
	return 0;
}