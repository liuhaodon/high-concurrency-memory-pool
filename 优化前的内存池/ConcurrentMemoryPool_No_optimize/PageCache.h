#pragma once
#include"Common.h"

class PageCache
{
public:
	static PageCache* GetPageInstance()
	{
		return &_PageCacheInstacne;
	}
	Span* NewSpan(size_t size);
	std::mutex& GetMutex();
	Span* GetObjToSpanMap(void* obj);
	//回收来自中心缓存的Span
	void ReleaseSpanToPageCache(Span* span);
private:
	SpanList _spanlists[MAXPAGEBUCKETS];
	std::mutex _PageMutex;//一把锁锁住所有的桶
	std::unordered_map<PAGE_ID, Span*> _idSpanMap;//id映射Span
private:
	PageCache() {}
	PageCache(const PageCache&) = delete;
	static PageCache _PageCacheInstacne;
};