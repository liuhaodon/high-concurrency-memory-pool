#include"ThreadCache.h"
void* ThreadCache::GetFromCentralCache(size_t index, size_t size)
{
	//中心缓存
	//满开始反馈调节算法
	size_t alignNum = min(_FL[index].MaxSize(), SizeMapClass::TdGetForCenCache(size));//申请的内存块
	//慢慢增长alignNum
	//size越大，一次向central cache申请的alignNum就越小
	//size越小，一次向central cache申请的alignNum就越大
	if (alignNum == _FL[index].MaxSize())
	{
		_FL[index].MaxSize()++;
	}
	void* start = nullptr;
	void* end = nullptr;
	//实际获得的span->freeList中链接的内存块数量
	size_t actualNum = CentralCache::GetInstance()->FetchRangeObj(start, end, alignNum, size);
	assert(actualNum);

	if (actualNum == 1)
	{
		assert(start == end);
		return start;
	}
	else
	{
		_FL[index].PushRandge(FreeList::NextObj(start), end, actualNum - 1);
		return start;
	}
	return nullptr;
}
void* ThreadCache::AllocMemory(size_t size)
{
	assert(size <= MAX_BYTES);
	//对齐后申请的内存字节
	size_t alignSize = SizeMapClass::AlignSize(size);
	//计算在哪个哈希桶
	size_t index = SizeMapClass::AlignBucket(size);
	//桶不为空
	if (!_FL[index].empty())
	{
		return _FL[index].PopAllocateMem();
	}
	else
	{
		//去中心缓存获取
		return GetFromCentralCache(index, alignSize);
	}
}
void ThreadCache::DelAllocMemory(void* obj, size_t size)
{
	assert(obj);
	assert(size <= MAX_BYTES);
	/*找对自由链表的对应的桶*/
	size_t index = SizeMapClass::AlignBucket(size);
	_FL[index].PushAllocMem(obj);

	//当freelist链接的内存块过长时，归还ThreadCache中挂载的内存块
	if (_FL[index].size() >= _FL[index].MaxSize())
	{
		ListToLong(_FL[index], size);
	}
}
void ThreadCache::ListToLong(FreeList& list, size_t size)
{
	void* start = nullptr;
	void* end = nullptr;
	list.PopRange(start, end, list.MaxSize());
	//归还到中心缓存
	CentralCache::GetInstance()->ReleaseListToSpans(start, size);
}