#pragma once
#include"Common.h"
#include"ThreadCache.h"

static void* ConcurrentAlloc(size_t size)
{
	if (pTLSThreadCache == nullptr)
	{
		pTLSThreadCache = new ThreadCache();
	}
	return pTLSThreadCache->AllocMemory(size);
}
static void ConcurrentFree(void* obj, size_t size)
{
	assert(pTLSThreadCache);
	pTLSThreadCache->DelAllocMemory(obj, size);
}
