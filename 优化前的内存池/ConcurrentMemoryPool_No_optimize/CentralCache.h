#pragma once

#include"Common.h"

class CentralCache
{
public:
	static CentralCache* GetInstance()
	{
		return &_OnlyInstance;
	}
	//从中心缓存获取一定数量的对象给thread cache
	size_t FetchRangeObj(void*& start, void*& end, size_t alignnum, size_t size);
	//获取一个非空的Span
	Span* GetOnceSpan(SpanList& sps, size_t size);
	//回收来自ThreadCache中归还的内存
	void ReleaseListToSpans(void* start, size_t bytes);
private:
	CentralCache() {}
	CentralCache(const CentralCache&) = delete;
	SpanList _splists[MAXFREELIST];
private:
	static CentralCache _OnlyInstance;
};