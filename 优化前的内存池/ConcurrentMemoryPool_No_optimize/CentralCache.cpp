#pragma once
#include"CentralCache.h"
#include"PageCache.h"
//定义static成员变量
CentralCache CentralCache::_OnlyInstance;
Span* CentralCache::GetOnceSpan(SpanList& sps, size_t size)
{
	//先在sps中查询span的freeList中有无内存块
	Span* it = sps.begin();
	while (it != sps.end())
	{
		if (it->_freeList != nullptr) return it;
		it = it->_next;
	}
	//先解锁,如果同时有线程要归还内存不会被锁住
	sps._mutex.unlock();
	//遍历完所有的Span没找到，就去PageCache中申请内存
	//给PageCacche加锁
	PageCache::GetPageInstance()->GetMutex().lock();
	Span* span = PageCache::GetPageInstance()->NewSpan(SizeMapClass::PageGetForOs(size));
	//该span已经被使用，状态置为true
	span->_isUse = true;
	PageCache::GetPageInstance()->GetMutex().unlock();

	//找到span的起始地址
	//通过页号 << 3 找到起始地址
	char* start = (char*)(span->_pageId << PAGE_SHIFT);
	size_t Sumbytes = span->_n << PAGE_SHIFT;
	char* end = start + Sumbytes;
	//切分span,挂载自由链表上,该过程不需要加锁
	//尾插
	span->_freeList = static_cast<void*>(start);
	start += size;
	void* tail = span->_freeList;
	while (start < end)
	{
		FreeList::NextObj(tail) = start;
		tail = start;
		start += size;
	}
	//插入前需要加锁
	sps._mutex.lock();
	sps.PushFront(span);

	return span;
}
size_t CentralCache::FetchRangeObj(void*& start, void*& end, size_t alignNum, size_t size)
{
	assert(alignNum);
	size_t indexBucket = SizeMapClass::AlignBucket(size);
	//加锁
	_splists[indexBucket]._mutex.lock();

	Span* span = GetOnceSpan(_splists[indexBucket], size);

	assert(span && span->_freeList);
	start = span->_freeList;
	end = start;
	//如果alignNum > Span中的freeList链接的块数,有多少给多少。
	size_t i = 0, actualNum = 1;
	while (i < alignNum - 1 && FreeList::NextObj(end) != nullptr)
	{
		end = FreeList::NextObj(end);
		++i;
		++actualNum;
	}
	span->_freeList = FreeList::NextObj(end);
	span->_useCount += actualNum;
	//解锁
	_splists[indexBucket]._mutex.unlock();
	return actualNum;
}
//将ThreadCache归还的内存链接到中心缓存的span中
void CentralCache::ReleaseListToSpans(void* start, size_t bytes)
{
	size_t index = SizeMapClass::AlignBucket(bytes);
	//加锁
	_splists[index]._mutex.lock();
	while (start)
	{
		void* next = FreeList::NextObj(start);
		Span* span = PageCache::GetPageInstance()->GetObjToSpanMap(start);
		//往span的freelist中进行链接内存块
		FreeList::NextObj(start) = span->_freeList;
		span->_freeList = start;
		span->_useCount--;
		if (span->_useCount == 0)
		{
			//往PageCache中去归还
			_splists[index].erase(span);//先解开span的链接
			//清空span，除了页号和页数
			span->_next = span->_prev = nullptr;
			span->_freeList = nullptr;
			//解centralcache的桶锁
			_splists[index]._mutex.unlock();
			//给pageache加锁
			PageCache::GetPageInstance()->GetMutex().lock();
			//归还到PageCache中
			PageCache::GetPageInstance()->ReleaseSpanToPageCache(span);
			PageCache::GetPageInstance()->GetMutex().unlock();
			//重新加上centralcache的桶锁
			_splists[index]._mutex.lock();
		}
		start = next;
	}
	_splists[index]._mutex.unlock();

}